#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "lectorBmp.h"
#include "filtros.h"

#define min(a,b) (((a) < (b)) ? (a) : (b))
#define max(a,b) (((a) > (b)) ? (a) : (b))

/*
 * Funciones en asembler
 */
// DOC_HELP
extern int asmPrint
(char * msg, int lenght);

int asm_Print(char * msg){
  return asmPrint(msg,strlen(msg));
}

extern void blendAsm(int length, int* red, int* green, int* blue,
int* red2, int* green2, int* blue2);

void multiplyBlend(int length, int* red, int* green, int* blue,
int* red2, int* green2, int* blue2){
	blendAsm(length, red, green, blue, red2, green2, blue2);
}

void doMutiplyBlendASM(int length, int* arrayRed1, int* arrayGreen1, int* bluarrayBlue1,
int* arrayRed2, int* arrayGreen2, int* arrayBlue2, BMPDATA* bmpData);

void doMutiplyBlendSSEIntrinsics(int length, int* red, int* green, int* blue,
int* red2, int* green2, int* blue2, BMPDATA* bmpData);
/******************************************************************************/

int main (int argc, char* argv[]) {
	asm_Print("Organización del Computador 2.\nTrabajo Práctico Nro. 2\nPrograma para procesamiento de imágenes BMP.\n");
	int resolucion = 1080;
	BMPDATA bmpData;
	BMPDATA bmpDataToBlend;
	int i;

	// carga el archivo bmp
	if (loadBmpFile ("blend1.bmp", &bmpData) != 0) {

		printf ("Error al leer el archivo %s\n\n", "blend1.bmp");
		return 1;
	}


	//blancoYNegro (&bmpData);
	//aclarar(&bmpData,50);
	//medianFilter(bmpData.red,bmpData.red, bmpData.infoHeader.biWidth, bmpData.infoHeader.biHeight);
	//medianFilter(bmpData.green,bmpData.green, bmpData.infoHeader.biWidth, bmpData.infoHeader.biHeight);
	//medianFilter(bmpData.blue, bmpData.blue, bmpData.infoHeader.biWidth, bmpData.infoHeader.biHeight);

	// carga el archivo bmp para blendear
	if (loadBmpFile ("blend2.bmp", &bmpDataToBlend) != 0) {

		printf ("Error al leer el archivo %s\n\n", "blend2.bmp");
		return 1;
	}

	//preparo los array para los metodos de blend
	int cant = cantPixels(&bmpData);
	int arrayRed1[cant];
	int arrayGreen1[cant];
	int arrayBlue1[cant];
	for (int i = 0; i < cant; i++)
	{
		arrayRed1[i]=bmpData.red[i];
		arrayGreen1[i]=bmpData.green[i];
		arrayBlue1[i]=bmpData.blue[i];
	}

	int cant2 = cantPixels(&bmpDataToBlend);
	int arrayRed2[cant2];
	int arrayGreen2[cant2];
	int arrayBlue2[cant2];
	for (int i = 0; i < cant2; i++)
	{
		arrayRed2[i]=bmpDataToBlend.red[i];
		arrayGreen2[i]=bmpDataToBlend.green[i];
		arrayBlue2[i]=bmpDataToBlend.blue[i];
	}

	clock_t start, end;
	// comienza a medir el tiempo
	start = clock();

	//invertir(&bmpData);

	//doMutiplyBlendASM(cantPixels(&bmpData),arrayRed1, arrayGreen1, arrayBlue1, arrayRed2, arrayGreen2, arrayBlue2, &bmpData);
	//doMutiplyBlendSSEIntrinsics(cantPixels(&bmpData),arrayRed1, arrayGreen1, arrayBlue1, arrayRed2, arrayGreen2, arrayBlue2, &bmpData);
	end = clock();
	FILE *out = fopen("results.csv", "a");  
	int tiempo = end-start;
	// imprime tiempo
	fprintf(out, "%d %s %d\n", resolucion, " tiempo: ", tiempo );
  	fclose(out); 

	printf("\nTiempo de proceso: %ld ticks.\n\n", end-start);

	if (saveBmpFile ("blend1.bmp", &bmpData) != 0)
		asm_Print("Error al grabar el archivo!");
	
	// libera memoria
	limpiarBmpData(&bmpData);

		if (saveBmpFile ("blend2.bmp", &bmpDataToBlend) != 0)
		asm_Print("Error al grabar el archivo!");
	
	// libera memoria
	limpiarBmpData(&bmpDataToBlend);
	return 0;
}

void doMutiplyBlendASM(int length, int* arrayRed1, int* arrayGreen1, int* arrayBlue1,
int* arrayRed2, int* arrayGreen2, int* arrayBlue2, BMPDATA* bmpData)
{
	multiplyBlend(cantPixels(bmpData),arrayRed1, arrayGreen1, arrayBlue1, arrayRed2, arrayGreen2, arrayBlue2);
	for (int i = 0; i < cantPixels(bmpData); i++)
	{
		bmpData->red[i]=min(arrayRed1[i],255);
		bmpData->green[i]=min(arrayGreen1[i],255);
		bmpData->blue[i]=min(arrayBlue1[i],255);
	}
}

void doMutiplyBlendSSEIntrinsics(int length, int* arrayRed1, int* arrayGreen1, int* arrayBlue1,
int* arrayRed2, int* arrayGreen2, int* arrayBlue2, BMPDATA* bmpData)
{
	int minBoundaries = cantPixels(bmpData);
	int destination[minBoundaries];
	int destination2[minBoundaries];
	int destination3[minBoundaries];
	multiplyParallel(arrayRed1,arrayRed2, destination, minBoundaries);
	multiplyParallel(arrayGreen1,arrayGreen2, destination2, minBoundaries);
	multiplyParallel(arrayBlue1,arrayBlue2, destination3, minBoundaries);
	for (int i = 0; i < minBoundaries; i++)
	{
		bmpData->red[i]=min(destination[i],255);
		bmpData->green[i]=min(destination2[i],255);
		bmpData->blue[i]=min(destination3[i],255);
	}
}