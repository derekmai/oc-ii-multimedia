# OC II - Multimedia

OC II (Organizador del Computador II). Trabajo sobre procesamiento multimedia en ASM y C. Se desarrolla algoritmos en ambos lenguajes, y se hace linking entre ambos.
Se debían desarrollar varios algoritmos de manipulación de imágenes.

- **Aclarado** Antes y después de ejecutar el método en 'C' aclarar.

## Pre ejecución
![](/screenshots/lena.png)

## Post ejecución
![](/screenshots/lenaAfter.png)

- **Median filter** Antes y después de ejecutar el método en 'C' medianFilter.

## Pre ejecución
![](/screenshots/sample3.png)

## Post Ejecución
![](/screenshots/sample3After.png)

- *Multiply Blend** Antes y después de ejecutar el método en 'ASM' multiplyBlend .

## Se hizo blend de estas dos imágenes sobre la primera.
### Blend1
![](/screenshots/blend1.png)
### Blend2
![](/screenshots/blend2.png)
## Post Ejecucción
![](/screenshots/blend1After.png)

## Código ASM
![](/screenshots/cuerpoblendasm.png)
## Código ASM, dentro del 'for'
![](/screenshots/mulBlend.png)

## Su contra cara en 'C', Usando SSE Intrinsics
![](/screenshots/multiplyParallel.png)

## Comparación en términos de ejecucción entre el método hecho en ASM y SSE Intrinsics

- En el root del proyecto hay dos archivos: "resultsASM.csv" & "resultsSSE.csv".
- Se hizo una muestra de 50 ejecuciones de cada método.
- A pesar de estar usando Intrinsics, el promedio de ASM fue mejor (1314.98 ticks). El de Intrinsics fue de 1683.98 ticks
- Cabría esperar que si las instrucciones multimedia se hubiesen usado directamente en Assembler en vez de en C, el promedio del de operaciones multimedia fuera menor.

## Bonus

### Invertir imágen
- Se hizo el método para invertir imágenes en C.

## Pre ejecución
![](/screenshots/lena.png)

## Post ejecución
![](/screenshots/lenaAfterInvert.png)

### Aclarar en ASM
- Mientras se debugeaba el método de multiply blend, se realizó el método aclarar.

![](/screenshots/aclararasm.png)

## Aclaraciones
- Se usó int[] en vez de unsigned char[] en los métodos de multiply blend, por problemas reiterados en la parte de pasaje de parámetros en ASM y por el tamaño de los paquetes en SSE.
- Por la estructura del desarrollo de los algoritmos, no se puede manipular imagenes de resoluciones muy elevadas (Se tomó esa decisión de diseño porque escapaba del scope del trabajo).
