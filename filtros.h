#ifndef FILTROS_H
#define FILTROS_H

#include "bmp.h"
#define min(a,b) (((a) < (b)) ? (a) : (b))
#define max(a,b) (((a) > (b)) ? (a) : (b))
// intel simd
#if defined(__SSE2__)
#include <emmintrin.h>
#endif
#include <xmmintrin.h>
typedef struct tagFILTRO {

	float RR, RG, RB;
	float GR, GG, GB;
	float BR, BG, BB;
} FILTRO;

FILTRO SEPIA;

void filtro (BMPDATA*, FILTRO);
void blancoYNegro (BMPDATA*);
int cantPixels (BMPDATA*);
unsigned char mediana (unsigned char*, int);
void medianFilter(unsigned char *image, unsigned char *result, int N, int M);
void _medianfilter(unsigned char *image, unsigned char *result, int N, int M);
void invertir(BMPDATA*);
void multiplyParallel(int *channel1,int *channel2,int *destination, int length);
void mulTest( int *channel1, int *channel2, int *destination, int length);
__m128i div255_epu16(__m128i value);
void aclarar (BMPDATA*, int n);
#endif
