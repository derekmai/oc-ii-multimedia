#include "filtros.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

FILTRO SEPIA = { 
	.393, .760, .189,
	.349, .686, .168,
	.272, .534, .131
};

/******************************************************************************/

void filtro (BMPDATA *bmpData, FILTRO filtro) {

	for (int i=0; i<cantPixels(bmpData); i++) {

		unsigned char r = bmpData->red[i];
		unsigned char g = bmpData->green[i];
		unsigned char b = bmpData->blue[i];
		bmpData->red[i]   = min (r * filtro.RR + g * filtro.RG + b * filtro.RB, 255);
		bmpData->green[i] = min (r * filtro.GR + g * filtro.GG + b * filtro.GB, 255);
		bmpData->blue[i]  = min (r * filtro.BR + g * filtro.BG + b * filtro.BB, 255);
	}
}

/******************************************************************************/

void blancoYNegro (BMPDATA *bmpData) {
	
	for (int i=0; i<cantPixels(bmpData); i++) {
	
		unsigned char y = bmpData->red[i] * 0.11448f + bmpData->green[i] * 0.58661f + bmpData->blue[i] * 0.29891f;
		bmpData->red[i]   = y;
		bmpData->green[i] = y;
		bmpData->blue[i]  = y;
	}	
}

/******************************************************************************/

unsigned char mediana (unsigned char *histo, int imediana) {

	int k, aux=0;
	for (k=0; k<255 && aux<=imediana; k++)
		aux += histo[k];

	return k;
}

/******************************************************************************/

int cantPixels(BMPDATA *bmpData) {

	return bmpData->infoHeader.biWidth * bmpData->infoHeader.biHeight;
}

//este anda lo más bien
void aclarar (BMPDATA *bmpData, int n)
{
	for (int i=0; i<cantPixels(bmpData); i++) {
		bmpData->red[i]= min (bmpData->red[i] + n,255);
		bmpData->green[i]= min (bmpData->green[i] + n,255);
		bmpData->blue[i]= min (bmpData->blue[i] + n,255);
	}
}
	
void medianFilter(unsigned char *image, unsigned char* result, int N, int M)
{
   if (!image || N < 1 || M < 1)
      return;
   unsigned char* extension [(N + 2) * (M + 2)];
   if (!extension)
      return;
   //   creo la extension
   for (int i = 0; i < M; ++i)
   {
      memcpy(extension + (N + 2) * (i + 1) + 1,
         image + N * i,
         N * sizeof(unsigned char));
      extension[(N + 2) * (i + 1)] = &image[N * i];
      extension[(N + 2) * (i + 2) - 1] = &image[N * (i + 1) - 1];
   }
   //   lleno la primer parte de la extension
   memcpy(extension,
      extension + N + 2,
      (N + 2) * sizeof(unsigned char));
   //   lleno la ultima parte de la extension
   memcpy(extension + (N + 2) * (M + 1),
      extension + (N + 2) * M,
      (N + 2) * sizeof(unsigned char));
   _medianfilter(*extension, result ? result : image, N + 2, M + 2);
}

void _medianfilter(unsigned char* image, unsigned char* result, int N, int M)
{
   //   Muevo la ventana por cada pixel
   for (int x = 1; x < M - 1; x++)
      for (int y = 1; y < N - 1; y++)
      {
         //   levanto los valores de la ventana
         int k = 0;
         int window[9];
         for (int j = x - 1; j < x + 2; j++)
            for (int i = y - 1; i < y + 2; i++)
               window[k++] = image[x * N + i];
         //ordenamos la lista de menor a mayor
         for (int j = 0; j < 5; j++)
         {
            int min = j;
            for (int l = j + 1; l < 9; l++)
            if (window[l] < window[min])
               min = l;
            const int temp = window[j];
            window[j] = window[min];
            window[min] = temp;
         }
         //pongo el del medio
         result[x*N + y] = window[4];
      }
}

void invertir (BMPDATA *bmpData)
{
	for (int i=0; i<cantPixels(bmpData); i++)
   {
		bmpData->red[i]= 255 - bmpData->red[i];
		bmpData->green[i]= 255 - bmpData->green[i];
		bmpData->blue[i]= 255 - bmpData->blue[i];
	}
}

void multiplyParallel( int *channel1,int *channel2, int*destination, int length)
{
   __m128i channel1Var;
   __m128i channel2Var;
   __m128i result;
   #pragma omp parallel for
   for (int i = 0; i<length; i+=4)
   {
         channel1Var = _mm_loadu_si128( (const __m128i*)&channel1[i] );
         channel2Var = _mm_loadu_si128( (const __m128i*)&channel2[i] );
         result = _mm_mullo_epi16 (channel1Var, channel2Var);
         result = div255_epu16(result);
         _mm_storeu_si128 ( (__m128i *)&destination[i], result );
   }
   bool extra = length % 4 == 0;
   if (extra)
   {
      for (int i = length -4; i < length; i++)
      {
            destination[i]=channel1[i]*channel2[i];
      }
   }
}

__m128i div255_epu16(__m128i value) {
    __m128i mulhi = _mm_mulhi_epu16(value, _mm_set1_epi16(0x8081));
    return _mm_srli_epi16(mulhi, 7);
}
